PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin
DOCDIR ?= $(PREFIX)/share/doc

install:

	install -d $(DESTDIR)$(BINDIR)
	install -t $(DESTDIR)$(BINDIR) -m 755 bin/mdadm-notify-handler
	install -t $(DESTDIR)$(BINDIR) -m 755 bin/notify-all
	install -t $(DESTDIR)$(BINDIR) -m 755 bin/smartd-notify-handler

	install -d $(DESTDIR)$(DOCDIR)/notify-scripts
	install -t $(DESTDIR)$(DOCDIR)/notify-scripts -m 644 COPYING
	install -t $(DESTDIR)$(DOCDIR)/notify-scripts -m 644 README

uninstall:

	rm -f  $(DESTDIR)$(BINDIR)/mdadm-notify-handler
	rm -f  $(DESTDIR)$(BINDIR)/notify-all
	rm -f  $(DESTDIR)$(BINDIR)/smartd-notify-handler

	rm -rf $(DESTDIR)$(DOCDIR)/notify-scripts

.PHONY: install uninstall

#            :indentSize=3:tabSize=3:noTabs=false:mode=makefile:maxLineLen=78: